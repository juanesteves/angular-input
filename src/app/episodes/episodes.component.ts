import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss']
})
export class EpisodesComponent implements OnInit {
  @Input() episodes!: any
  selection!: string

  constructor () {}

  ngOnInit (): void {}
  onCityClicked (episode: string): void {
    console.log('Evente ->', episode)
    this.selection = episode
  }

  onClear (): void {
    this.selection = ''
  }
}
