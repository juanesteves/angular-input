import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'app-grid-cards',
  templateUrl: './grid-cards.component.html',
  styleUrls: ['./grid-cards.component.scss']
})
export class GridCardsComponent implements OnInit {
  @Input() data!: any

  constructor () {}

  ngOnInit (): void {
    console.log('grid -> ', this.data)
  }
}
